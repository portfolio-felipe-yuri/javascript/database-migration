const Const = require("../constants/constants");
const StringUtils = require("./string.utils");
const Path = require("path");

module.exports = {
  resolveOriginalChannelCanary: resolveOriginalChannelCanary,
  processDbscriptFilePath: processDbscriptFilePath,
  processOs: processOs,
  processChannel: processChannel,
  processClassifier: processClassifier,
  processMavenRepository: processMavenRepository
};

function resolveOriginalChannelCanary(branchName) {
  const ABREV_CHANNEL_INDEX = 2;
  const REGEX = /(\w+)(?:-(ga|rc|dev|([0-9]+.[0-9]+.x))-([\w-]+))?/;

  return branchName.match(REGEX)[ABREV_CHANNEL_INDEX];
}

function processDbscriptFilePath() {
  return Const.BUILD_FOLDER_SCRIPTS.concat(Path.sep).concat(Const.DBSCRIPT_FILENAME);
}

function processOs(branchName) {
  StringUtils.checkNullString(branchName);

  return branchName.replaceAll(Const.REGEX_NOT_NUMBER, "");
}

function processChannel() {
  if (Const.BRANCH_NAME == Const.RELEASE_NAME_GA) {
    return Const.ABREV_GA;
  }

  if (Const.BRANCH_NAME == Const.RELEASE_NAME_RC) {
    return Const.ABREV_RC;
  }

  if (Const.BRANCH_NAME == Const.RELEASE_NAME_DEV) {
    return Const.ABREV_DEV;
  }

  return Const.CANARY;
}

function processClassifier() {
  const channel = processChannel();

  switch (channel) {
    case Const.ABREV_GA:
      return Const.EMPTY;
    case Const.ABREV_RC:
      return Const.RELEASE_NAME_RC;
    case Const.ABREV_DEV:
      return Const.RELEASE_NAME_DEV;

    default:
      return Const.CANARY;
  }
}

function processMavenRepository() {
  if ([Const.RELEASE_NAME_GA, Const.RELEASE_NAME_RC].indexOf(Const.BRANCH_NAME) > -1) {
    return "maven-releases";
  }
  return "maven-snapshots";
}
