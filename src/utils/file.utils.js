const ShellUtils = require("./shell.utils");
const StringUtils = require("./string.utils");
const Path = require("path");
const fs = require("fs-extra");

module.exports = {
  getFileSize: function (filePath) {
    StringUtils.checkNullString(filePath);

    return ShellUtils.execCommand(`du -h ${filePath} | awk -F' ' '{print $1}'`);
  },

  processFileName: function (filePath) {
    const INDEX_LAST_BAR = filePath.lastIndexOf(Path.sep) + 1;
    const LENGTH_PATH = filePath.length;

    return filePath.substring(INDEX_LAST_BAR, LENGTH_PATH);
  },

  addFilesInExistsZip: function (zipFile, fileToAdd) {
    StringUtils.checkNullInArray([zipFile, fileToAdd]);

    return ShellUtils.execCommand(`zip ${zipFile} -j ${fileToAdd}`);
  },

  packFilesInZip: function (source, fileName, regex) {
    StringUtils.checkNullInArray([source, fileName, regex]);

    return ShellUtils.execCommand(
      `cd ${source} && zip ${fileName} -j ${regex}`
    );
  },

  processContentFile: processContentFile
};

async function processContentFile(pathFile) {
  return fs.readFileSync(pathFile, {encoding: "binary"} , convertEncodeToISO);
}

async function convertEncodeToISO(err, data) {
  const ENCODE_ISO_8859_1 = "ISO-8859-1";

  return iconv.decode(data, ENCODE_ISO_8859_1);
}


