const Const = require("../constants/constants");
const ShellUtils = require("./shell.utils");
const Path = require("path");

module.exports = {
  processTagByVersion: function () {
    return ShellUtils.execCommand(
      `cd ${Const.PROJECT_DIR} && git tag -l --sort=-version:refname | head -n 1`
    );
  },

  processTagByDate: function () {
    const latestTag = ShellUtils.execCommand(
      `cd ${Const.PROJECT_DIR} && git for-each-ref --format="%(taggerdate): %(refname)" --sort=-taggerdate --count=1 refs/tags`
    );
    const INDEX_LAST_BAR = latestTag.lastIndexOf(Path.sep) + 1;
    const LEN_TAG = latestTag.length;

    return latestTag.substring(INDEX_LAST_BAR, LEN_TAG);
  },

  processHash: function (tag) {
    return ShellUtils.execCommand(`cd ${Const.PROJECT_DIR} && git show-ref -s tags/${tag} | head -n 1`);
  }
};
