const execSync = require("child_process").execSync;
const StringUtils = require("./string.utils.js");

module.exports = {
  execCommand: function (cmd) {
    StringUtils.checkNullString(cmd);

    const ENCODE = { encoding: "utf-8" };

    return execSync(cmd, ENCODE).trim();
  },

  exportEnvVariable: function (key, value) {
    this.execCommand(`echo ${key}=${value} >> build.env`);
  }
};


