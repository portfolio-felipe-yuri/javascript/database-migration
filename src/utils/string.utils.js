const TYPE_STRING = "string";

module.exports = {
  checkNullInArray: function (arr) {
    if (!arr || arr.length === 0 || !Array.isArray(arr)) {
      throw new Error(`O parâmetro ${arr} é nulo ou não é do tipo 'array'!`);
    }

    arr.forEach((str) => {
      this.checkNullString(str);
    });
  },

  checkNullObject: function (obj) {
    if (!obj || Object.keys(obj).length === 0 || !Object.getPrototypeOf(obj) === Object.prototype) {
      throw new Error(`O parâmetro ${obj} é nulo ou não é do tipo 'object'!`);
    }
  },

  checkNullString: function (str) {
    if (!str  || !typeof(str) === TYPE_STRING) {
      throw new Error(`O parâmetro ${str} é nulo ou não do tipo 'string'!`);
    }
  },

  getVarName: function (variable) {
    return Object.keys(variable)[0];
  }
};
