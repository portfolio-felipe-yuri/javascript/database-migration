const DataDictionaryFactory = require("./controllers/datadictionary.migration.controller");
const DataDictionaryService = require("./services/datadictionary.migration.service");

async function execute() {
    try{
        await DataDictionaryFactory.buildMigration();
    
        DataDictionaryService.uploadMigration();
    } catch (error) {
      throw new Error("O ocorreu um erro ao publicar dicionário de dados", { cause: error });
    }
}

execute();
