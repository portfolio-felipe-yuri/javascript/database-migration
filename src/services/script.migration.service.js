const StringUtils = require("../utils/string.utils");
const Const = require("../constants/constants");
const Serviceproxy = require("./serviceproxy.service");

module.exports = {
  insertMigration: insertMigration
};

async function insertMigration(body) {
  StringUtils.checkNullString(body);

  const url = `${process.env.HOST_PACKAGE_REPOSITORY_API}/${process.env.URL_V1}/${process.env.SERVICE_INSERT_MIGRATION}`;
  
  let config = {
    headers: {
      Authorization: `Bearer ${Const.PACKAGE_REPOSITORY_TOKEN}`
    }
  };

  return await Serviceproxy.callService(url, body, config);
}