const { S3Client } = require("@aws-sdk/client-s3");
const { fromEnv } = require("@aws-sdk/credential-providers");
const Const = require("../constants/constants");
const credentials = fromEnv();

const s3Client = new S3Client({
  region:             Const.ORACLE_REGION,
  endpoint:           Const.ORACLE_BUCKET_URL,
  apiVersion:         Const.ORACLE_V4,
  useGlobalEndpoint:  true,
  forcePathStyle:     true,
  credentials
});

module.exports = { s3Client };
