const Const = require("../constants/constants");
const ShellUtils = require("../utils/shell.utils");
const SnkUtils = require("../utils/snk.utils");

module.exports = {
  uploadMigration: uploadMigration
};

async function uploadMigration() {
  const _classifier = SnkUtils.processClassifier();
  const _repository = SnkUtils.processMavenRepository();

  const nexusService = `curl -v -u ${Const.NEXUS_USER}:${Const.NEXUS_PASSWORD} \
      -F 'maven2.generate-pom=true' \
      -F 'maven2.groupId=${Const.PACKAGE_GROUP}' \
      -F 'maven2.artifactId=${Const.DD_ARTIFACT_ID}' \
      -F 'maven2.packaging=zip' \
      -F 'version=${Const.SEMANTIC_TAG}' \
      -F 'maven2.asset1=@${Const.DD_BUILD_FOLDER}/${Const.DD_FILENAME_ZIP}' \
      -F 'maven2.asset1.classifier=${_classifier}' \
      -F 'maven2.asset1.extension=zip' '${Const.NEXUS_HOST}/service/rest/v1/components?repository=${_repository}'`;

  ShellUtils.execCommand(nexusService);
}


