const axios = require("axios");

module.exports = {
  callService: async function (url, body, config) {
    const HTTP_SUPPORT_METHODS = ["POST", "PUT", "DELETE", "GET"];

    var method = "POST";

    if (config && config.method) {
      method = config.method;
      method = method.toUpperCase();

      if (HTTP_SUPPORT_METHODS.indexOf(method) == -1) {
        throw new Error("O método não é suportado. Utilizar post, put, delete ou get");
      }
    }

    const requestBody = {
      method: method,
      url: url,
      headers: { "Content-Type": "application/json" },
      data: body
    };

    if (config && config.headers) {
      for (var key in config.headers) {
        requestBody.headers[key] = config.headers[key];
      }
    }

    let resp;

    await axios(requestBody).then(function (response) {
      console.log(`Serviço executado com sucesso para a url: ${url}`);

      resp = response;

    }).catch(function (error) {
      throw new Error(`Erro ao executar o serviço para a url: ${url}`, { cause: error });
    });

    return resp;
  }
};
