const Const = require("./constants/constants");
const SnkUtils = require("./utils/snk.utils");
const BucketControl = require("./controllers/bucket.controller");
const PackageControl = require("./controllers/packageRepository.controller");
const MigrationControl = require("./controllers/script.migration.controller");
const fs = require("fs");

function checkExistScripts() {
    try {
        fs.statSync(Const.BUILD_FOLDER_SCRIPTS);
        return true;
    } catch (err) {
        if (err.code === "ENOENT") {
            console.log("file or directory does not exist");
        }
        return false;
    }
}

function loadVariables() {
    const data = {};

    try {

        data.tag = Const.SEMANTIC_TAG;
        data.channel = SnkUtils.processChannel();
        data.hash = Const.CURRENT_HASH;
        data.currentDateIso = Const.CURRENT_DATE_ISO;
        data.bucketFilePath = BucketControl.buildBucketFilePath(data.channel);
        data.url = BucketControl.buildBucketFileLink(data.bucketFilePath);
        data.branch = SnkUtils.processOs(Const.BRANCH_NAME);
        data.dbscriptFilePath = SnkUtils.processDbscriptFilePath();

    } catch (error) {
        throw new Error("Erro ao tentar carregar variáveis de entrada!", { cause: error });
    }

    return data;
}

async function resolveBucket(params) {
    MigrationControl.buildMigration(params);
    await BucketControl.sendToBucket(params.channel);
}

async function resolvePackageRepository(params) {
    await PackageControl.sendToPackageRepository(params);
}

async function execute() {
    try {
        if (!checkExistScripts()) {
            console.warn("Não existe nenhum arquivo de script para ser publicado!");
            return;
        }

        const params = loadVariables();

        console.log("Parâmetros de entrada:", params);

        await resolveBucket(params);
        await resolvePackageRepository(params);
    } catch (error) {
        throw new Error("O ocorreu um erro ao publicar script de migration", { cause: error });
    }
}

execute();