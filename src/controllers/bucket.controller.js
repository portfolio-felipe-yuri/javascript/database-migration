const Const = require("../constants/constants");
const StringUtils = require("../utils/string.utils");
const Path = require("path");
const { s3Client } = require("../services/s3.client.service");
const { PutObjectCommand } = require("@aws-sdk/client-s3");
const fs = require("fs");

module.exports = {
  sendToBucket : sendToBucket,
  buildBucketFileLink : buildBucketFileLink,
  buildBucketFilePath : buildBucketFilePath
};

async function sendToBucket(channel) {
  StringUtils.checkNullString(channel);

  const filePath = buildBucketFilePath(channel);

  await processCopyFile(filePath);
}

async function processCopyFile(destDir) {
  StringUtils.checkNullString(destDir);

  const originFilePath = Const.BUILD_FOLDER_SCRIPTS.concat(Path.sep).concat(Const.DBSCRIPT_FILENAME);

  const requestParams = {
    Bucket: Const.ORACLE_BUCKET_REPOSITORY,
    Body: fs.readFileSync(originFilePath),
    Key: destDir
  };

  try {
    console.log("Enviado migration para o bucket...");

    const results = await s3Client.send(new PutObjectCommand(requestParams));

    console.log(results);
    console.log("Migration ".concat(destDir).concat(" enviado com sucesso!"));
  }  catch (error) {
    throw new Error("Erro ao tentar copiar os migrations para o bucket.", {cause: error});
  }
}

function buildBucketFilePath(channel) {
  StringUtils.checkNullString(channel);

  return `erp/${channel}/package/database/dbscript`.concat(Path.sep).concat(Const.DBSCRIPT_FILENAME);
}

function buildBucketFileLink(path) {
  StringUtils.checkNullString(path);

  //TODO: Usar esse bloco quando o DNS for possível de ser acessado dentro da rede da company (fisicamente na company)
  // return OCI_LINK.concat(path.replace("storage-repository", ""));

  return Const.ORACLE_BUCKET_URL.concat(Path.sep).concat(Const.ORACLE_BUCKET_REPOSITORY).concat(Path.sep).concat(path);
}
