const FileUtils = require("../utils/file.utils");
const ShellUtils = require("../utils/shell.utils");
const StringUtils = require("../utils/string.utils");
const SnkUtils = require("../utils/snk.utils");
const Const = require("../constants/constants");
const { create } = require("xmlbuilder2");

module.exports = {
  buildMigration: buildMigration
};

async function buildMigration() {
  try{
    const dataMigration = await buildZipMigrationObj();
  
    buildFileMigration(dataMigration);
  
    const REGEX_SQL_JSON = "*.xml *.json";
  
    FileUtils.packFilesInZip(
      Const.DD_BUILD_FOLDER,
      Const.DD_FILENAME_ZIP,
      REGEX_SQL_JSON
    );
  } catch (error) {
    throw new Error("O ocorreu um erro ao empacotar Dicionário de dados", { cause: error });
  }
}

async function buildZipMigrationObj() {
  const data = {
    [Const.SNKMODULE]: {
      "migration": Const.BUILD,
      "import": Const.DD_IMPORT_DATE,
      "export": Const.DD_EXPORT_DATE,
      "source": Const.DD_ID,
      "export_host": Const.DD_HOST,
      "version": Const.SEMANTIC_TAG,
      "hash_change": Const.CURRENT_HASH,

      "export_version": await processExportVersion(),
      "channel": SnkUtils.processChannel()
    }
  };

 console.log("Objeto compactado:", data);

  return data;
}

function buildFileMigration(data) {
  StringUtils.checkNullObject(data);

  data = JSON.stringify(data);

  const migrationFileName = "dd-migration.json";

  ShellUtils.execCommand(
    `echo '${data}' | jq . > ${Const.DD_BUILD_FOLDER}/${migrationFileName}`
  );

  return ShellUtils.execCommand(`readlink -f ${migrationFileName}`);
}

async function processExportVersion() {
  const xmlFilePath = `${Const.DD_BUILD_FOLDER}/${Const.DD_FILENAME_XML}`;
  const xmlContent = await FileUtils.processContentFile(xmlFilePath);

  let resultRegex = xmlContent.match(/ver_exportador="(\d+\.\d+\.\d+)"/);
 
  if(resultRegex){
      return resultRegex[1];
  }

  throw new Error("Não foi possível obter a versão do exportador!");

}
