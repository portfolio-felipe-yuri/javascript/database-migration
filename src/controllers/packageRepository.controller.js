const MigrationService = require("../services/script.migration.service");
const MigrationControl = require("../controllers/script.migration.controller");
const StringUtils = require("../utils/string.utils");

module.exports = {
  sendToPackageRepository: sendToPackageRepository
};

async function sendToPackageRepository(params) {
  StringUtils.checkNullObject(params);

  const migrationBody = MigrationControl.buildInsertMigrationObj(params);

  await MigrationService.insertMigration(migrationBody);
}
