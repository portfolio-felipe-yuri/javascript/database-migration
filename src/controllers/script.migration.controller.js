const StringUtils = require("../utils/string.utils");
const FileUtils = require("../utils/file.utils");
const Const = require("../constants/constants");
const fs = require("fs");

module.exports = {
  buildMigration: buildMigration,
  buildInsertMigrationObj: buildInsertMigrationObj
};

function buildMigration(params) {
  StringUtils.checkNullObject(params);
  
  const REGEX_SQL = "*.sql";

  try {

    if (fs.readdirSync(Const.BUILD_FOLDER_SCRIPTS).length === 0) {
      console.log("Não existe nenhum arquivo de migration para ser publicado!");
  
      process.exit(0);
    }
    
    FileUtils.packFilesInZip(Const.BUILD_FOLDER_SCRIPTS, Const.DBSCRIPT_FILENAME, REGEX_SQL);

  } catch (error) {
    throw new Error("Erro ao tentar montar pacote com os migrations (.sql)", { cause: error });
  }


}

function buildInsertMigrationObj(params) {

  try {
    const data = {
      "migration":    Const.BUILD,
      "module":       Const.SNKMODULE,
      "branch":       Const.BRANCH_NAME,

      "channel":      params.channel,
      "version":      params.tag,
      "hash_change":  params.hash,
      "published":    params.currentDateIso,
      "url":          params.url
    };

    console.log("Objeto para ser inserido no Package Repository:", data);

    return data;
  } catch (error) {
    throw new Error("Erro ao tentar montar o objeto de migration que vai para o package repository", { cause: error });
  }
}